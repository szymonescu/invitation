import json

import falcon


class InvitationResource:
    def on_get(self, req, resp):
        resp.body = json.dumps(
            [{"invitee": "John Smith", "email": "john@smith.mx"}])

    def on_post(self, req, resp):
        resp.status = falcon.HTTP_201
