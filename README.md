# Motivation
This task was very simple and easy and using a robust framework like pyramid or (dev-god forbid) django
would be an overkill, so I've decided to find a lightweight one. In other projects in my company we use *falcon* as
a framework and I've heard a lot of good things about it so I've decided to pick it as my base, although I did not
work with it before. My server choice was *gunicorn*, mostly because it is what they suggest in the falcon tutorial.

# Summary
I've created one resource with two methods and wrote tests for it. I've skipped any validation or error handling,
because it was such a small project.

# Installation
- cd into the directory containing this file
- run pip install -r requirements.txt
- run run.sh

# Tests
- cd into the directory containing this file
- run pip install -r requirements_tests.txt
- run py.test test_invitation.py
