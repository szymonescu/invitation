import falcon

from invitation import InvitationResource

invitation = InvitationResource()
app = falcon.API()

app.add_route('/invitation', invitation)
