import json

import pytest
from falcon import testing

from app import app


@pytest.fixture(scope='module')
def client():
    return testing.TestClient(app)


def test_get_invitation_returns_list_of_objects(client):
    doc = [{"email": "john@smith.mx", "invitee": "John Smith"}]

    result = client.simulate_get('/invitation')
    assert result.status_code == 200
    assert result.json == doc

def test_post_returns_201(client):
    body = {"email": "john@smith.mx", "invitee": "John Smith"}

    result = client.simulate_post('/invitation', body=json.dumps(body))
    assert result.status_code == 201
